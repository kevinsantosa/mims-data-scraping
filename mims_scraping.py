import requests

# BS4
from bs4 import BeautifulSoup

# SELENIUM
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import time
import csv
import re
import unicodedata

print('import success')

####################################

### Define global variables
main_url = 'https://www.mims.com'
list_browse_drugs_urls = [
    '/indonesia/browse/alphabet/a?cat=drug',
    '/indonesia/browse/alphabet/b?cat=drug',
    '/indonesia/browse/alphabet/c?cat=drug',
    '/indonesia/browse/alphabet/d?cat=drug',
    '/indonesia/browse/alphabet/e?cat=drug',
    '/indonesia/browse/alphabet/f?cat=drug',
    '/indonesia/browse/alphabet/g?cat=drug',
    '/indonesia/browse/alphabet/h?cat=drug',
    '/indonesia/browse/alphabet/i?cat=drug',
    '/indonesia/browse/alphabet/j?cat=drug',
    '/indonesia/browse/alphabet/k?cat=drug',
    '/indonesia/browse/alphabet/l?cat=drug',
    '/indonesia/browse/alphabet/m?cat=drug',
    '/indonesia/browse/alphabet/n?cat=drug',
    '/indonesia/browse/alphabet/o?cat=drug',
    '/indonesia/browse/alphabet/p?cat=drug',
    '/indonesia/browse/alphabet/q?cat=drug',
    '/indonesia/browse/alphabet/r?cat=drug',
    '/indonesia/browse/alphabet/s?cat=drug',
    '/indonesia/browse/alphabet/t?cat=drug',
    '/indonesia/browse/alphabet/u?cat=drug',
    '/indonesia/browse/alphabet/v?cat=drug',
    '/indonesia/browse/alphabet/w?cat=drug',
    '/indonesia/browse/alphabet/x?cat=drug',
    '/indonesia/browse/alphabet/y?cat=drug',
    '/indonesia/browse/alphabet/z?cat=drug'
]

list_urls = [main_url + url for url in list_browse_drugs_urls]
print(list_urls)

# Variable to store all the drug informations
all_drugs_info = []

####################################

## Clean string
def clean_string(x):
    x = x.replace('\"','\'\'').replace('\r',' ').replace('\n',' ')
    x = unicodedata.normalize('NFKD', x).encode('ascii', 'ignore')
    x = x.decode('ascii')

    return x

## Scraping MIMS data
def mims_scraping(): 
    for url in list_urls:
        # res = requests.get('https://www.mims.com/indonesia/browse/alphabet/a?cat=drug', headers={'User-Agent': 'Mozilla/5.0'})
        res = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
        soup = BeautifulSoup(res.text, 'html.parser')

        browse_drugs_section = soup.find('div', class_='browse-drugs')
        list_drugs = browse_drugs_section.ul.find_all('li')
        # print(list_drugs)

        print('===============================')
        x = re.match('.+\/(\w)\?cat=drug', str(url))
        alphabet = x.group(1)
        print('Number of drugs starting with \'{}\' alphabet: '.format(alphabet.upper()) + str(len(list_drugs))) 

        for i in range(len(list_drugs)):
        # for i in range(5):
            print('-----------------------')
            ## Get each drug link
            temp = list_drugs[i].find('a', href=True)
            drug_link = temp['href']
            full_drug_link = main_url + drug_link
            print('Go to ' + str(full_drug_link)) 
            
            ## Go to drug link
            res2 = requests.get(full_drug_link, headers={'User-Agent': 'Mozilla/5.0'})
            soup_drug = BeautifulSoup(res2.text, 'html.parser')

            ### Start getting data
            # Get Drug Type
            # NOTES: drug type need to be scraped first in order to scrape the other data since Generic drug and Brand drug have different web structure
            drug_type = list_drugs[i]['data-type'].strip()
            print('Drug Type: ' + str(drug_type))

            if drug_type == 'Brand':
                ## Get Drug Name & Manufacturer
                drug_name_manufacturer = soup_drug.find('div', class_='swap-order')
                
                # Get Drug Name
                drug_name = drug_name_manufacturer.find('div', class_='div-brand').text.strip()
                print('Drug Name: ' + str(drug_name))

                # Get Manufacturer
                try:
                    drug_manufacturer = drug_name_manufacturer.find('div', {'style': 'margin-top:20px;'}).text.strip()
                    x = re.match("Manufacturer:\n(.+)", str(drug_manufacturer))
                    temp = x.group(1)
                    drug_manufacturer = temp
                except:
                    drug_manufacturer = '-'
                print('Drug Manufacturer: ' + str(drug_manufacturer))

                ## Get Contents, MIMS Class, ATC
                drug_info_section = soup_drug.find('div', class_='monograph-content')
                every_info = drug_info_section.find_all('div', attrs={'id': True})

                ## Loop through all the drug information
                for info in every_info:
                    # Take drug info header first to specify which info is the content/mims class/atc
                    drug_info_header = info.find('div', class_='monograph-section-header').text.strip()

                    # Get Contents
                    if 'Contents' in drug_info_header:
                        drug_contents = info.find('div', class_='monograph-section-content').text.strip()
                        print('Drug Contents: ' + str(drug_contents))
                    # Get MIMS Class 
                    elif 'MIMS Class' in drug_info_header:
                        drug_class = info.find('div', class_='monograph-section-content').text.strip()
                        print('Drug Class: ' + str(drug_class))
                    # Get ATC
                    elif 'ATC Classification' in drug_info_header:
                        drug_atc_classification = info.find('div', class_='monograph-section-content').text.strip()
                        drug_atc_classification = drug_atc_classification.replace('"', '')
                        drug_atc_classification = drug_atc_classification.replace(' ;', '.')
                        # drug_atc_classification = clean_string(drug_atc_classification)
                        print('Drug ATC: ' + str(drug_atc_classification))

                    # # Get Packing Information (Packages)
                    # if 'Presentation/Packing' in drug_info_header:
                    #     drug_packing = info.find('div', class_='monograph-section-content').text.strip()
                    #     drug_packing = drug_packing.replace('"', '')
                    #     drug_packing = drug_packing.replace(' ;', '.')
                    #     # drug_packing = clean_string(drug_packing)
                    #     print('Drug Packing: ' + str(drug_packing))
                    # else:
                    #     drug_packing = '-'
            
            elif drug_type == 'Generic':
                ## Get Drug Name
                drug_name = soup_drug.find('h1', class_='druginfo-header').text.strip()
                print('Drug Name: ' + str(drug_name))

                # ## Get Manufacturer (most likely generic drugs does not give manufacturer info)
                # try:
                #     drug_manufacturer = drug_name_manufacturer.find('div', {'style': 'margin-top:20px;'}).text.strip()
                #     print(drug_manufacturer)
                #     y = re.match("Manufacturer:\n(.+)", str(drug_manufacturer))
                #     temp2 = y.group(1)
                #     drug_manufacturer = temp2
                # except:
                #     drug_manufacturer = '-'
                
                # Generic drugs does not provide Manufacturer info
                drug_manufacturer = '-'
                print('Drug Manufacturer: ' + str(drug_manufacturer))

                ## Get Contents, MIMS Class, ATC
                drug_info_section = soup_drug.find('div', class_='monograph-content')
                every_info = drug_info_section.find_all('div', attrs={'id': True})

                drug_contents = '-'
                drug_class = '-'
                drug_atc_classification = '-'

                ## Loop through all the drug information
                for info in every_info:
                    # Take drug info header first to specify which info is the content/mims class/atc
                    drug_info_header = info.find('div', class_='monograph-section-header').text.strip()

                    # Get Contents
                    if 'Contents' in drug_info_header:
                        drug_contents = info.find('div', class_='monograph-section-content').text.strip()
                        print('Drug Contents: ' + str(drug_contents))
                    # Get MIMS Class 
                    elif 'MIMS Class' in drug_info_header:
                        drug_class = info.find('div', class_='monograph-section-content').text.strip()
                        print('Drug Class: ' + str(drug_class))
                    # Get ATC
                    elif 'ATC Classification' in drug_info_header:
                        drug_atc_classification = info.find('div', class_='monograph-section-content').text.strip()
                        drug_atc_classification = drug_atc_classification.replace('"', '')
                        drug_atc_classification = drug_atc_classification.replace(' ;', '.')
                        # drug_atc_classification = clean_string(drug_atc_classification)
                        print('Drug ATC: ' + str(drug_atc_classification))

            data = {
                'Drug Name': drug_name,
                'Drug Manufacturer': drug_manufacturer,
                'Drug Contents': drug_contents,
                'Drug Class': drug_class,
                'ATC Classification': drug_atc_classification,
                'Drug Type': drug_type
            }

            all_drugs_info.append(data)
            write_to_csv(all_drugs_info)

## Write the result to CSV file
def write_to_csv(all_product):
    output_filename = 'drugs_mims_data_final_v2.csv'
    fieldnames = [
        'Drug Name',
        'Drug Manufacturer',
        'Drug Contents',
        'Drug Class',
        'ATC Classification',
        'Drug Type'
    ]

    with open(output_filename, 'w', newline='', encoding='utf-8') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for row in all_product:
            writer.writerow(row)

## Main function
def main():
    try:
        mims_scraping()
    except Exception as err:
        print('ERROR: \n' + str(err))

if __name__ == '__main__':
    main()


### WHAT WILL BE SCRAPED: 
# drug name
# manufacturer
# contents 
# mims class
# atc
# generic or brand



